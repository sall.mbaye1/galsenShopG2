/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package sn.edu.ipsl.galsenshop.metier;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import sn.edu.ipsl.galsenshop.dao.CategorieFacade;
import sn.edu.ipsl.galsenshop.model.Categorie;

/**
 *
 * @author mjoo
 */
@Named(value = "ajouterCategorieBean")
@SessionScoped
public class AjouterCategorieBean implements Serializable{

    /**
     * Creates a new instance of AjouterCategorieBean
     */
    
    @EJB
    private CategorieFacade cf;
    
    private Categorie cat = new Categorie();
    
    /*private String code;
    
    private String nom;
    
    private String description;*/
    
    private float tva;    
    

    public CategorieFacade getCf() {
        return cf;
    }

    public void setCf(CategorieFacade cf) {
        this.cf = cf;
    }

    public Categorie getCat() {
        return cat;
    }

    public void setCat(Categorie cat) {
        this.cat = cat;
    }

    /*public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/

    public float getTva() {
        return tva;
    }

    public void setTva(float tva) {
        this.tva = tva;
    }
    
    public String enregistrer(){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            cat.setTva((float)getTva());
            cf.create(cat);
            System.out.println("categorie ajoutée");
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AjouterCategorieBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "index.html?faces-redirect=true";
    }
    
    
}
