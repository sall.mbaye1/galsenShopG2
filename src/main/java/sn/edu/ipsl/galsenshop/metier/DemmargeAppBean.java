/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB31/SingletonEjbClass.java to edit this template
 */
package sn.edu.ipsl.galsenshop.metier;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sn.edu.ipsl.galsenshop.dao.CategorieFacade;
import sn.edu.ipsl.galsenshop.dao.ClientFacade;
import sn.edu.ipsl.galsenshop.model.Categorie;
import sn.edu.ipsl.galsenshop.model.Client;

/**
 *
 * @author mjoo
 */
@Singleton
@Startup
public class DemmargeAppBean {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @EJB
    private CategorieFacade cf;
    
    
    public DemmargeAppBean(){
        System.out.println("### initialisation de l'app #### ");
    }

    public CategorieFacade getCf() {
        return cf;
    }

    public void setCf(CategorieFacade cf) {
        this.cf = cf;
    }
    
    @PostConstruct
    public void init() {
        System.out.println("### test Base Donnée ###");
        
        Categorie cat1 = new Categorie("BSE", "biens sociaux essentiels",
                "les médicaments, appareils médico-chirurgicaux, le papier journal, les livres, les journaux, les fauteuils roulants, certains engrais etc");
        
        Categorie cat2 = new Categorie("BPN", "biens de première nécessité",
                "les matières premières de base, les biens d’équipement et les intrants spécifiques",
                (float)0.05);
        
        Categorie cat3 = new Categorie("IPI", "intrants et produits intermédiaires",
                " ",
                (float)0.1);
         Categorie cat4 = new Categorie("BCF", "biens de consommation finale",
                "catégorie par defaut",
                (float)0.2);
         cf.create(cat1);
         cf.create(cat2);
         cf.create(cat3);
         cf.create(cat4);
        System.out.println("etudiant ajoutÃ©");
    }
    
}
