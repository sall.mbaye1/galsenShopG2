/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sn.edu.ipsl.galsenshop.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;

/**
 *
 * @author mjoo
 */
@Entity
public class Categorie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String code;
    
    @Column
    private String nom;
    
    
    @Column
    private String description;
    
    @Column
    private Float tva;
    
    
    @PrePersist
    public void initTva(){
        if(getTva()==null){
            setTva((float)0.07);
        }
    }

    public Categorie() {
    }

    public Categorie(String code, String nom, String description) {
        this.code = code;
        this.nom = nom;
        this.description = description;
    }
    
    

    public Categorie(String code, String nom, String description, Float tva) {
        this.code = code;
        this.nom = nom;
        this.description = description;
        this.tva = tva;
    }
    
    

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getTva() {
        return tva;
    }

    public void setTva(Float tva) {
        this.tva = tva;
    }
    
    
    
}
